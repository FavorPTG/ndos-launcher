<p textAlign="center"><h1>Скрипт полуавтоматической установки ArchLinux</h1></p>

<br>
<p><h2>Как использовать NDos launcher</h2></p>
Перед установкий необходимо разметить диск. Можете сделать это самостоятельно или воспользоваться комплектным скриптом  'mandm'. Он предложит оптимальные варианты разметки и правильно примонтирует все разделы диска.<br>
<br>Чтобы активировать 'mandm' введите:

```shell
./mandm
```

<br>Для установки системы на размеченный диск введите:<br>

```shell
./ndl -i {DiskName: /dev/sdX} Username Password(together for user and root) {Video driver: intel, amd, nvidia, none} {Desktop environment: kde, gnome, xfce, xonly, none}
```


<p><h5>Пояснения по использованию скрипта</h5></p>
Первый флаг `-i` указывает на то, что вы собираетесь именно установить операционную систему (на данный момент других функций нет, но они есть в планах).<br><br>

Второй пункт - имя диска, на который будет установлена система. Посмотреть его можно следующей командой:

```shell
fdisk -l
```

**_!!!Внимание!!! Имя диска следует вводить именно как /dev/DiskName_**

Третий пункт - имя основного пользователя. <br>
**_!!!Внимание!!! Имя пользователя должно быть написано строчными буквами_**<br><br>

Четвёртый пункт - пароль для пользователя.<br> 
**_Примечание: пароль распространяется как на нового пользователя, так и на супер-пользователя. Ести вас это не устраивает, можете поменять его после установки._**<br><br>

Пятый пункт - драйвера для видеокарт. Вы можете выбрать один из трёх драйверов: для intel, nvidia или amd. Есть возможность не устанавливать друйвера, прописав 'none'.<br> 
**_Примечание: данная версия скрипта ещё не поддерживает установку драйверов для нескольких видеокарт._**<br><br>

Шестой пункт - выбор окружения рабочего стола. Можно выбрать одно из трёх: gnome, kde или xfce. Можно, также, установить комплект X11 для дальнейшей установки оконного менеджера. Или можно ничего не устанавливать, прописав 'none'.

<h4>Пример готовой команды</h4>

```shell
./ndl -i /dev/vda test 123 intel kde
```

<br><br>
<p><h3>Преимущества NDos launcher</h3></p>
1) Быстрая установка системы ArchLinux (около 10 минут)<br>
2) Автоматическая установка драйверов для видеокарт<br>
3) Возможность установить пакет Xorg для дальнейшей установки оконных менеджеров<br>
<br>
<br>
<p><h3>Дорожная карта проекта</h3></p>
1) Реализовать функцию автоматической установки Plymouth.<br>
2) Добавить вариант установки видеодрайверов для гибридной графики.<br>
3) Добавить вариант интерактивной установки, идущей по пунктам.
<br>
<br>
<p><h2>Схемы разметок диска</h2></p>
<br>

**UEFI/GPT:**

|          Mount:         | Partition: |       Size:     |
| ----------------------- | ---------- | --------------- |
| /boot или<br>/boot/efi  | /dev/sda1  | min 300MiB      |
| [SWAP]                  | /dev/sda2  | min 512MiB      |
| /                       | /dev/sda3  | rest            |
<br>

**BIOS/MBR:**

|   Mount:   | Partition: |            Size:            |
| -----------| ---------- | --------------------------- |
| [SWAP]     | /dev/sda1  | min 512MiB                  |
| /          | /dev/sda2  | rest                        |
| no mount   |    none    | min 16.5KiB<br>(1 MiB auto) |
<br>

**BIOS/GPT:**

|    Mount:   | Partition: |       Size:     |
| ------------| ---------- | --------------- |
| no mount    | /dev/sda1  | min 31MiB       |
| [SWAP]      | /dev/sda2  | min 512MiB      |
| /           | /dev/sda3  | rest            |

<br>
<br>
<p><h3>Инструкция разметки диска через Parted</h3></p>
<br>

```shell
parted mkpart part-type-or-part-label fs-type start end
```

dos: part-type-or-part-label = part-type<br>
gpt: part-type-or-part-label = label<br>
<br>
fs-type: not create file system!!!<br>
<br>
start: start of partition<br>
end: end of partition<br>
<br>
<br>
<br>
**FOR BIOS/GPT** <br>
```shell
parted /dev/sda mklabel gpt
parted /dev/sda mkpart "Boot" fat32 31MiB 331MiB
parted /dev/sda mkpart "Root" btrfs 331MiB 100%
```
<br>

**FOR UEFI/GPT** <br>
```shell
parted /dev/sda mklabel gpt
parted /dev/sda mkpart "Boot" fat32 0% 300MiB
parted /dev/sda mkpart "Root" btrfs 300MiB 100%
```
<br>

**FOR BIOS/DOS** <br>
```shell
parted /dev/sda mklabel mbr
parted /dev/sda mkpart primary btrfs 1MiB 100%
```
<br>

**FOR CREATE SWAP**<br>
```shell
parted /dev/sda mkpart linux-swap start end
parted /dev/sda mklabel gpt mkpart P1 ext3 1MiB 8MiB
``` 
